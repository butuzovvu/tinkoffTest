package com.tinkoff.service;

import com.tinkoff.dto.RequestDto;
import com.tinkoff.feign.TranslateFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslateServiceImpl implements TranslateService {

    @Value("${feign.key}")
    private String KEY;

    private final String SEPARATOR = "\\s";

    private final TranslateFeign translateFeign;

    @Autowired
    public TranslateServiceImpl(TranslateFeign translateFeign) {
        this.translateFeign = translateFeign;
    }

    @Override
    public String getTranslate(RequestDto requestDto) {

        if (requestDto == null || requestDto.getText() == null || requestDto.getFrom() == null || requestDto.getTo() == null) {
            throw new IllegalArgumentException("Пустое значение входных параметров");
        }

        List<String> words = getWords(requestDto.getText());

        String lang = getLang(requestDto.getFrom(), requestDto.getTo());

        return words.parallelStream().map(w -> getTranslateWord(w, lang)).collect(Collectors.joining(" "));
    }

    private String getTranslateWord(String word, String lang) {
        log.info("Старт перевода слова: " + word + ". Направление перевода: " + lang + ". Время: " + LocalTime.now());
        return translateFeign.getTranslate(KEY, lang, word).getText();
    }

    private String getLang(String from, String to) {
        return from + "-" + to;
    }

    private List<String> getWords(String text) {
        return Pattern.compile(SEPARATOR)
                .splitAsStream(text)
                .collect(Collectors.toList());
    }
}
