package com.tinkoff.feign;

import com.tinkoff.dto.ResponseDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "yandex", url = "${feign.url}")
public interface TranslateFeign {

    @RequestMapping(value = "/translate", method = RequestMethod.GET, produces = "application/json")
    ResponseDto getTranslate(@RequestParam("key") String key, @RequestParam("lang") String lang, @RequestParam("text") String text);
}
